﻿using NW.Service.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace NW.Service
{
    [ServiceContract(Namespace = "http://lean-stack.de/northwind", CallbackContract = typeof(OrderState))]
    public interface OrderFulfillment
    {
        [OperationContract(IsOneWay = true)]
        void HandleOrder(OrderHeader order);
    }

    [ServiceContract(Namespace = "http://lean-stack.de/northwind")]
    public interface OrderState
    {
        [OperationContract(IsOneWay = true)]
        void Report(string state);
    }
}
