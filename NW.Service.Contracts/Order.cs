﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace NW.Service
{
    [ServiceContract(Namespace = "http://lean-stack.de/northwind", SessionMode = SessionMode.Required)]
    public interface Order
    {
        [OperationContract(IsInitiating = true)]
        void CreateOrder(string customerId);

        [OperationContract(IsInitiating = false)]
        void AddItemToOrder(int productId, int quantity);

        [OperationContract(IsInitiating = false, IsTerminating = true)]
        void PlaceOrder();
    }
}
