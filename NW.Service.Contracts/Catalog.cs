﻿using NW.Service.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace NW.Service
{
    [ServiceContract(Namespace = "http://lean-stack.de/northwind")]
    public interface Catalog
    {
        [OperationContract]
        List<CatalogCategory> GetCategories();

        [OperationContract]
        List<CatalogProduct> GetProductsByCategory(int categoryId);
    }
}
