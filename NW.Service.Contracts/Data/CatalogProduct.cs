﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NW.Service.Data
{
    [DataContract(Namespace = "http://lean-stack.de/northwind")]
    public class CatalogProduct
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int CategoryId { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public decimal UnitPrice { get; set; }

        [DataMember]
        public string Unit { get; set; }
    }
}
