﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NW.Service.Data
{
    [DataContract(Namespace = "http://lean-stack.de/northwind")]
    public class OrderHeader
    {
        [DataMember]
        public string CustomerId { get; set; }

        [DataMember]
        public List<OrderItems> Items { get; set; }
    }
}
