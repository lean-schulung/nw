﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NW.Service;
using NW.Service.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;

namespace NW.Service.Tests
{
    [TestClass()]
    public class CatalogCacheImplTests
    {
        [TestInitialize]
        public void SetupCache()
        {
            var cache = MemoryCache.Default;

            var mockCategories = new CatalogCategory[] {
                new CatalogCategory() { Id =1, Name="Getränke" },
                new CatalogCategory() { Id =2, Name="Fleisch" }
            };
            cache.Add(typeof(CatalogCategory).FullName, mockCategories,
                new CacheItemPolicy() { SlidingExpiration = TimeSpan.FromHours(1) }
                );
        }

        [TestMethod()]
        public void GetCategoriesTest()
        {
            var svc = new CatalogCacheImpl();
            var items = svc.GetCategories();
            Assert.IsTrue(items.Count() > 0);
        }

        [TestMethod()]
        public void InvalidateCategoriesCacheTest()
        {
            var svc = new CatalogCacheImpl();
            svc.InvalidateCategoriesCache();
            Assert.IsNull(MemoryCache.Default.Get(typeof(CatalogCategory).FullName));
        }
    }
}