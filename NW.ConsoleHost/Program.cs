﻿using NW.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Messaging;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace NW.ConsoleHost
{
    class Program
    {
        static void Main(string[] args)
        {
            // Create MessageQueue (funzt nicht, da Bindung Transaktionsunterstützung erwartet)
            if (!MessageQueue.Exists(@".\private$\orders")) MessageQueue.Create(@".\private$\orders",true);

            Type[] services = {
                typeof(CatalogCacheImpl),
                typeof(CatalogImpl),
                typeof(ProductInventoryImpl),
                typeof(OrderImpl),
                typeof(OrderFulfillmentImpl),
                typeof(OrderHandlerQueueImpl)
            };

            List<ServiceHost> hosts = new List<ServiceHost>();

            Console.WriteLine("Console Host");
            Console.WriteLine("============");
            Console.WriteLine();
            Console.WriteLine("Starting services ...");
            Console.WriteLine();

            foreach (var svc in services)
            {
                var host = new ServiceHost(svc);
                host.Open();

                Console.WriteLine("Service: "  + svc.Name);
                Console.WriteLine("Endpoints:");
                foreach (var ep in host.Description.Endpoints)
                {
                    Console.WriteLine("\tA: {0}, B: {1}, C: {2}",
                        ep.Address.ToString(),
                        ep.Binding.Name,
                        ep.Contract.Name
                    );
                }
                Console.WriteLine();

            }

            Console.WriteLine("Any key to stop services ...");
            Console.ReadKey();
        }
    }
}
