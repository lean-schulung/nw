﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using NW.Service.Data;

namespace NW.Service
{
    public class CatalogImpl : Catalog
    {
        CatalogCache _cacheService;

        public CatalogImpl()
        {
            var cf = new ChannelFactory<CatalogCache>("CatalogCache");
            _cacheService = cf.CreateChannel();
        }

        public List<CatalogCategory> GetCategories()
        {
            return _cacheService.GetCategories();
        }

        public List<CatalogProduct> GetProductsByCategory(int categoryId)
        {
            return _cacheService.GetProducts().Where(p => p.CategoryId == categoryId).ToList();
        }
    }
}
