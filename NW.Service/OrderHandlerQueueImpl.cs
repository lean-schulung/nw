﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NW.Service.Data;

namespace NW.Service
{
    public class OrderHandlerQueueImpl : OrderHandlerQueue
    {
        public void HandleOrder(OrderHeader order)
        {
            using (var sw = new StreamWriter("orders.txt",true))
            {
                sw.WriteLine("Order received for {0} at {1}", order.CustomerId, DateTime.Now.ToString());
                foreach (var item in order.Items)
                {
                    sw.WriteLine("Product: {0}, Quantity: {1}", item.ProductId, item.Quantity);
                }
                sw.WriteLine();
            }
        }
    }
}
