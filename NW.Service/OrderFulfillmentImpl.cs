﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using NW.Service.Data;

namespace NW.Service
{
    public class OrderFulfillmentImpl : OrderFulfillment
    {
        public void HandleOrder(OrderHeader order)
        {
            
            var callback = OperationContext.Current.GetCallbackChannel<OrderState>();

            var randomState = new Random().Next(1, 3);
            var state = randomState == 1 ? "Done" : (randomState == 2 ? "Postponed" : "Rejected");
            callback.Report(state);
        }
    }
}
