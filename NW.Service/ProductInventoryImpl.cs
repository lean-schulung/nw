﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using NW.Datalayer;
using NW.Service.Data;

namespace NW.Service
{
    [ServiceBehavior(InstanceContextMode=InstanceContextMode.PerCall)]
    public class ProductInventoryImpl : ProductInventory
    {
        NorthwindCtx _ctx = new NorthwindCtx();
        CatalogCache _cacheService;

        public ProductInventoryImpl()
        {
            var cf = new ChannelFactory<CatalogCache>("CatalogCache");
            _cacheService = cf.CreateChannel();
        }

        public List<InventoryProduct> GetProducts()
        {
            return _ctx.Products.Select(p =>
                new InventoryProduct()
                {
                    Id = p.ProductID,
                    Name = p.ProductName,
                    UnitPrice = p.UnitPrice ?? 0.0m,
                    Unit = p.QuantityPerUnit,
                    ReorderLevel = p.ReorderLevel ?? 0,
                    Discontinued = p.Discontinued
                }
            ).ToList();
        }

        public void UpdateProduct(InventoryProduct p)
        {
            var originalProduct = _ctx.Products.Find(p.Id);
            originalProduct.ProductName = p.Name;
            originalProduct.UnitPrice = p.UnitPrice;
            originalProduct.QuantityPerUnit = p.Unit;
            originalProduct.ReorderLevel = p.ReorderLevel;
            originalProduct.Discontinued = p.Discontinued;

            // TODO: Think about concurrency!
            _ctx.SaveChanges();
            _cacheService.InvalidateProductsCache();
        }
    }
}
