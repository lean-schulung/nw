﻿using NW.Service.Data;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace NW.Service
{
    public class OrderImpl : Order
    {
        OrderFulfillment _fulfillmentService;

        Dictionary<int, int> _items;
        string _customerId;

        public OrderImpl()
        {
            var callbackService = new OrderFulfillmentCallback();
            var cf = new DuplexChannelFactory<OrderFulfillment>(callbackService, "OrderFulfillment");
            _fulfillmentService = cf.CreateChannel();
        }

        public void CreateOrder(string customerId)
        {
            _customerId = customerId;
            _items = new Dictionary<int, int>();
        }

        public void AddItemToOrder(int productId, int quantity)
        {
            _items.Add(productId, quantity);
        }

        public void PlaceOrder()
        {
            var order = new OrderHeader() { CustomerId = _customerId };
            order.Items = _items.Select(it => new OrderItems() {
                ProductId = it.Key,
                Quantity = it.Value
            }).ToList();

            _fulfillmentService.HandleOrder(order);

            var cf = new ChannelFactory<OrderHandlerQueue>("OrderQueue");
            var queue = cf.CreateChannel();
            queue.HandleOrder(order);
        }
    }

    public class OrderFulfillmentCallback : OrderState
    {
        public void Report(string state)
        {
            // Handle state
            using (var sw = new StreamWriter("orders.log.txt"))
            {
                sw.WriteLine("({0}) Order State: {1}", DateTime.Now.ToString(), state);
            }
        }
    }
}
