﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;
using NW.Datalayer;
using NW.Service.Data;

namespace NW.Service
{
    public class CatalogCacheImpl : CatalogCache
    {
        private ObjectCache _cache = MemoryCache.Default;
        private NorthwindCtx _ctx = new NorthwindCtx();

        public List<CatalogCategory> GetCategories()
        {
            var items = GetCachedItems<CatalogCategory>();
            if (items == null)
            {
                items = _ctx.Categories.Select(c => new CatalogCategory() {
                    Id = c.CategoryID,
                    Name = c.CategoryName
                }).ToList();

                _cache.Add(
                    typeof(CatalogCategory).FullName, 
                    items,
                    new CacheItemPolicy() { SlidingExpiration = TimeSpan.FromHours(1) }
                );
            }
            return items;
        }

        public List<CatalogProduct> GetProducts()
        {
            var items = GetCachedItems<CatalogProduct>();
            if (items == null)
            {
                items = _ctx.Products
                    .Where(p => p.Discontinued == false)
                    .Where(p => p.CategoryID != null)
                    .Select(p => new CatalogProduct()
                        {
                        Id = p.ProductID,
                        CategoryId = (int) p.CategoryID,
                        Name = p.ProductName,
                        UnitPrice = p.UnitPrice ?? 0.0m,
                        Unit =  p.QuantityPerUnit
                    }).ToList();

                _cache.Add(
                    typeof(CatalogProduct).FullName,
                    items,
                    new CacheItemPolicy() { SlidingExpiration = TimeSpan.FromHours(1) }
                );
            }
            return items;
        }

        public void InvalidateCategoriesCache()
        {
            DeleteCache<CatalogCategory>();
        }

        public void InvalidateProductsCache()
        {
            DeleteCache<CatalogProduct>();
        }

        private List<T> GetCachedItems<T>()
        {
            var key = typeof(T).FullName;
            if (_cache.Contains(key))
            {
                return (List<T>)_cache.Get(key);
            }
            else
            {
                return null;
            }
        }

        private void DeleteCache<T>()
        {
            _cache.Remove(typeof(T).FullName);
        }
    }
}
