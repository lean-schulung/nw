﻿using NW.DemoConsole.CatalogRef;
using NW.Service;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace NW.DemoConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            // Proxy-Client für den Catalog Service
            var catalogSvc = new CatalogClient();

            // ChannelFactory für den Inventory Service
            var cf = new ChannelFactory<ProductInventory>("Inventory");
            var inventorySvc = cf.CreateChannel();

            var cf2 = new ChannelFactory<CatalogCache>("HttpCache");
            var httpCache = cf2.CreateChannel();
            var cf3 = new ChannelFactory<CatalogCache>("TcpCache");
            var tcpCache = cf3.CreateChannel();
            var cf4 = new ChannelFactory<CatalogCache>("PipeCache");
            var pipeCache = cf4.CreateChannel();

            pipeCache.GetProducts();

            benchmarkCache("tcp", tcpCache);
            benchmarkCache("pipe", pipeCache);
            benchmarkCache("http", httpCache);
            
        }

        static void benchmarkCache(string binding , CatalogCache svc)
        {
            Console.WriteLine(binding);
            var sum = 0L;
            var sw = new Stopwatch();

            for (int i = 0; i < 10; i++)
            {
                sw.Start();

                svc.GetProducts();

                sw.Stop();
                Console.WriteLine("Time: " + sw.ElapsedTicks);
                sum += sw.ElapsedTicks;
                sw.Reset();
            }

            Console.WriteLine("Average: " + (sum / 10));
        }
    }
}
